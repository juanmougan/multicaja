# Correr un query arbitrario a la DB Sqlite
require 'active_record'

@connection = ActiveRecord::Base.establish_connection(
            :adapter => "sqlite3",
            :host => "localhost",
            :database => "db/development.sqlite3"#,
            #:username => "root",
            #:password => "root123"
)

sql = "SELECT * from movements"
@result = @connection.connection.execute(sql);
@result.each(:as => :hash) do |row| 
   puts row["short_description"] 
end