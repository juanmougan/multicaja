class AddShortDescriptionToMovements < ActiveRecord::Migration
  def change
    # Seria el equivalente a una enum DEBITO/CREDITO
    add_column :movements, :short_description, :string
  end
end
