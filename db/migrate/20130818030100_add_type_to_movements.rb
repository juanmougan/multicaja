class AddTypeToMovements < ActiveRecord::Migration
  def change
    add_column :movements, :type, :string
  end
end
