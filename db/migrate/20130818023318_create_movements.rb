class CreateMovements < ActiveRecord::Migration
  def change
    create_table :movements do |t|
      t.decimal :amount
      t.datetime :moment
      t.integer :movementable_id
      t.string :movementable_type

      t.timestamps
    end
  end
end
