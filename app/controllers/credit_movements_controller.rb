class CreditMovementsController < MovementsController
  
  def new
     @credit_movement = CreditMovement.new
  end
  
  def create
    @credit_movement = CreditMovement.new(movement_params)

    respond_to do |format|
      if @credit_movement.save
        format.html { redirect_to @credit_movement, notice: 'Credit Movement was successfully created.' }
        format.json { render action: 'show', status: :created, location: @credit_movement }
      else
        format.html { render action: 'new' }
        format.json { render json: @credit_movement.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def movement_params
    params.require(:credit_movement).permit(:amount, :moment, :short_description)
  end
  
end
