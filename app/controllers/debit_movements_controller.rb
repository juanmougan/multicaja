class DebitMovementsController < MovementsController
  
  def new
     @debit_movement = DebitMovement.new
  end

  def create
    @debit_movement = DebitMovement.new(movement_params)

    respond_to do |format|
      if @debit_movement.save
        format.html { redirect_to @debit_movement, notice: 'Debit Movement was successfully created.' }
        format.json { render action: 'show', status: :created, location: @debit_movement }
      else
        format.html { render action: 'new' }
        format.json { render json: @debit_movement.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def index
  end

  #def show
  #end
  
  def movement_params
    params.require(:debit_movement).permit(:amount, :moment, :short_description)
  end
end
