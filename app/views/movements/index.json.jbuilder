json.array!(@movements) do |movement|
  json.extract! movement, :amount, :moment, :movementable_id, :movementable_type
  json.url movement_url(movement, format: :json)
end
